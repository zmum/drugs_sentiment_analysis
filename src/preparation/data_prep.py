import numpy as np
import pandas as pd

#%%
def load_data():
    train = pd.read_csv("data/raw/Train.csv", sep=";", error_bad_lines=False)
    test = pd.read_csv("data/raw/TestX.csv", sep=";", error_bad_lines=False)
    return train, test

#%%
train, test = load_data()
