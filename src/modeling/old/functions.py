import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt


def load_data():
    train = pd.read_csv("data/raw/Train.csv", sep=";", error_bad_lines=False)
    test = pd.read_csv("data/raw/TestX.csv", sep=";", error_bad_lines=False)
    return train, test


def prepare_inputs(X_train, X_test):
    X_train_enc, X_test_enc = list(), list()
    # label encode each column
    for i in range(X_train.shape[1]):
        le = LabelEncoder()
        # le.fit(X_train[:, i])
        le.fit(np.concatenate((X_train[:, i], X_test[:, i]), axis=0).astype(str))
        # encode
        train_enc = le.transform(X_train[:, i])
        test_enc = le.transform(X_test[:, i])
        # store
        X_train_enc.append(train_enc)
        X_test_enc.append(test_enc)
    return X_train_enc, X_test_enc


def train_val_split(train_data, uniques_rate=0.9):
    unique_opinions = pd.unique(train_data["opinion"].values)
    train = train_data.loc[train_data["opinion"].isin(unique_opinions[:int(unique_opinions.shape[0]*uniques_rate)])]
    val = train_data.loc[train_data["opinion"].isin(unique_opinions[int(unique_opinions.shape[0]*uniques_rate):])]
    return train, val


def decode_sentence(text, reverse_word_index):
    return ' '.join([reverse_word_index.get(i, '?') for i in text])


def plot_history(history):
    # Plot training & validation accuracy values
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()