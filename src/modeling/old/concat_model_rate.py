import numpy as np
import pandas as pd
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
import tensorflow as tf
import os
from sklearn.utils import class_weight

from modeling.old.functions import load_data, prepare_inputs, train_val_split, decode_sentence, plot_history


#%% define static vars
VOCAB_SIZE = 5000
EMB_DIM = 64
MAX_LEN = 400
TRUNC_TYPE = 'post'
PAD_TYPE = 'post'
OOV_TOK = '<OOV>'

#%% load and preprocess dataset
train, test = load_data()
train, test = train.dropna(), test.dropna()
train, val = train_val_split(train, uniques_rate=0.9)

train_posts, val_posts = train['opinion'], val['opinion']
X_train_enc, X_val_enc = prepare_inputs(train[["name", "condition"]].values, val[["name", "condition"]].values)
train_labels, val_labels = train['rate'], val['rate']
print("train shape: {}, val shape: {}".format(train.shape, val.shape))

#%% class weights calculate
class_weights = class_weight.compute_class_weight('balanced',
                                                  np.unique(train_labels),
                                                  train_labels)
print("class weights: ", class_weights)

#%% fit tokenizer on words in opinions
tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=VOCAB_SIZE, oov_token=OOV_TOK)
tokenizer.fit_on_texts(pd.concat((train_posts, val_posts), axis=0))
word_index = tokenizer.word_index
print("sample tokens: ", dict(list(word_index.items())[0:100]))

#%% tokenize words in opinions in train and val dataset
train_sequences = tokenizer.texts_to_sequences(train_posts)
train_padded = tf.keras.preprocessing.sequence.pad_sequences(train_sequences, maxlen=MAX_LEN,
                                                             padding=PAD_TYPE, truncating=TRUNC_TYPE)
validation_sequences = tokenizer.texts_to_sequences(val_posts)
validation_padded = tf.keras.preprocessing.sequence.pad_sequences(validation_sequences, maxlen=MAX_LEN,
                                                                  padding=PAD_TYPE, truncating=TRUNC_TYPE)
print("sample tokenized sentence: ", train_sequences[10])

#%% check tokenization
reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])
print("Original: ", train_posts[11])
print('---')
print("Tokenized then decoded: ", decode_sentence(train_padded[11], reverse_word_index))

#%% define recurrent part of model
in1 = tf.keras.layers.Input(shape=(400,))
h1 = tf.keras.layers.Embedding(VOCAB_SIZE, EMB_DIM)(in1)
h12 = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(EMB_DIM))(h1)
h13 = tf.keras.layers.Dense(EMB_DIM, activation='relu')(h12)

#%% define other parts of models (input and dense layers for condition and drug)
in_layers = list()
em_layers = list()
d_layers = list()
for i in range(len(X_train_enc)):
    # TODO: check if it is ok to know that there will be new drugs/conditions for embedding layer
    # calculate the number of unique inputs
    n_labels = len(np.unique(np.concatenate((X_train_enc[i], X_val_enc[i]), axis=0)))
    # define input layer
    in_layer = tf.keras.layers.Input(shape=(1,))
    # define embedding layer
    em_layer = tf.keras.layers.Embedding(n_labels, 10)(in_layer)
    # dense for concat
    dense_layer = tf.keras.layers.Flatten()(em_layer)
    # store layers
    in_layers.append(in_layer)
    em_layers.append(em_layer)
    d_layers.append(dense_layer)

d_layers.append(h13)
in_layers.append(in1)

#%% define final model
merge = tf.keras.layers.concatenate(d_layers)
dense = tf.keras.layers.Dense(100, activation='relu', kernel_initializer='he_normal')(merge)
output = tf.keras.layers.Dense(10, activation='softmax')(dense)
model = tf.keras.models.Model(inputs=in_layers, outputs=output)
model.summary()

#%% convert integers to dummy variables (i.e. one hot encoded)
train_labels_one_hot = tf.keras.utils.to_categorical(train_labels.values)[:, 1:]
val_labels_one_hot = tf.keras.utils.to_categorical(val_labels.values)[:, 1:]

#%% define callbacks
run_path = "model_weights/concat_rate"
# earlyStopping = EarlyStopping(monitor='val_accuracy', patience=10, verbose=0)
# mcp_save = ModelCheckpoint(os.path.join(run_path, 'model-{epoch:03d}-{accuracy:03f}-{val_accuracy:03f}.h5'),
#                            save_best_only=True, monitor='val_accuracy', patience=7)
mcp_save = ModelCheckpoint(os.path.join(run_path, 'model-weights_rate.h5'),
                           save_best_only=True, monitor='val_accuracy', patience=7)
reduce_lr_loss = ReduceLROnPlateau(monitor='val_accuracy', factor=0.1, patience=7, verbose=1, min_delta=1e-4)

# callbacks = [earlyStopping, mcp_save, reduce_lr_loss]
callbacks = [mcp_save, reduce_lr_loss]

#%% model training
# optional load previous weights
# model.load_weights("model_weights/concat/model-049-0.999215-0.645467.h5")

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

num_epochs = 15
history = model.fit([X_train_enc[0], X_train_enc[1], train_padded],
                    train_labels_one_hot,
                    epochs=num_epochs,
                    validation_data=([X_val_enc[0], X_val_enc[1], validation_padded], val_labels_one_hot),
                    verbose=1,
                    class_weight=class_weights,
                    # callbacks=callbacks,
                    batch_size=128)
plot_history(history)

#%%
predictions = model.predict([X_val_enc[0], X_val_enc[1], validation_padded])