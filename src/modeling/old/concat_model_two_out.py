import numpy as np
import pandas as pd
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import tensorflow as tf
from ast import literal_eval
# from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

import os


#%%
def load_data():
    train = pd.read_csv("data/raw/Train.csv", sep=";", error_bad_lines=False)
    test = pd.read_csv("data/raw/TestX.csv", sep=";", error_bad_lines=False)
    return train, test


def prepare_inputs(X_train, X_test):
    X_train_enc, X_test_enc = list(), list()
    # label encode each column
    for i in range(X_train.shape[1]):
        le = LabelEncoder()
        # le.fit(X_train[:, i])
        le.fit(np.concatenate((X_train[:, i], X_test[:, i]), axis=0))
        # encode
        train_enc = le.transform(X_train[:, i])
        test_enc = le.transform(X_test[:, i])
        # store
        X_train_enc.append(train_enc)
        X_test_enc.append(test_enc)
    return X_train_enc, X_test_enc


#%%
vocab_size = 5000
embedding_dim = 64
max_length = 400
trunc_type = 'post'
padding_type = 'post'
oov_tok = '<OOV>'
training_portion = .8


#%%
train, test = load_data()
all_data = train.where((pd.notnull(train)), '')

full_text = all_data['opinion']
names_cond = all_data[["name", "condition"]]

y1, y2 = all_data['rate'], all_data['rate1']
y2 = y2.replace("low", 0).replace("medium", 1).replace("high", 2)
print(np.median([len(elem) for elem in full_text]))
#%%
train_posts, val_posts = full_text[:135000], full_text[135000:]
train_labels1, val_labels1, train_labels2, val_labels2 = y1[:135000], y1[135000:], y2[:135000], y2[135000:]


X_train_enc, X_val_enc = prepare_inputs(names_cond[:135000].values, names_cond[135000:].values)

print(train_posts.shape, val_posts.shape)
print(train_labels1.shape, val_labels1.shape)
#%%
import pickle

# loading
with open('tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)
# tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=vocab_size, oov_token=oov_tok)
# tokenizer.fit_on_texts(full_text)
word_index = tokenizer.word_index
print(dict(list(word_index.items())[0:100]))


# # saving
# with open('tokenizer.pickle', 'wb') as handle:
#     pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)


#%%
train_sequences = tokenizer.texts_to_sequences(train_posts)
print(train_sequences[10])

#%%
train_padded = tf.keras.preprocessing.sequence.pad_sequences(train_sequences,
                                                             maxlen=max_length,
                                                             padding=padding_type,
                                                             truncating=trunc_type)
print(len(train_sequences[0]))
print(len(train_padded[0]))

print(len(train_sequences[1]))
print(len(train_padded[1]))

print(len(train_sequences[10]))
print(len(train_padded[10]))

#%%
validation_sequences = tokenizer.texts_to_sequences(val_posts)
validation_padded = tf.keras.preprocessing.sequence.pad_sequences(validation_sequences,
                                                                  maxlen=max_length,
                                                                  padding=padding_type,
                                                                  truncating=trunc_type)
print(len(validation_sequences))
print(validation_padded.shape)

#%%
reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])

def decode_article(text):
    return ' '.join([reverse_word_index.get(i, '?') for i in text])
print(decode_article(train_padded[11]))
print('---')
print(train_posts[11])

#%%

in1 = tf.keras.layers.Input(shape=(400,))
h1 = tf.keras.layers.Embedding(vocab_size, embedding_dim)(in1)
h12 = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(embedding_dim))(h1)
h13 = tf.keras.layers.Dense(embedding_dim, activation='relu')(h12)



#%%
in_layers = list()
em_layers = list()
d_layers = list()
for i in range(len(X_train_enc)):
    # TODO: check if it is ok to know that there will be new drugs/conditions for embedding layer
    # calculate the number of unique inputs
    n_labels = len(np.unique(np.concatenate((X_train_enc[i], X_val_enc[i]), axis=0)))
    # define input layer
    in_layer = tf.keras.layers.Input(shape=(1,))
    # define embedding layer
    em_layer = tf.keras.layers.Embedding(n_labels, 10)(in_layer)
    # dense for concat
    dense_layer = tf.keras.layers.Flatten()(em_layer)
    # store layers
    in_layers.append(in_layer)
    em_layers.append(em_layer)
    d_layers.append(dense_layer)

d_layers.append(h13)
in_layers.append(in1)
#%%
merge = tf.keras.layers.concatenate(d_layers)
dense = tf.keras.layers.Dense(20, activation='relu', kernel_initializer='he_normal')(merge)
output1 = tf.keras.layers.Dense(10, activation='softmax', name='rate_out')(dense)
output2 = tf.keras.layers.Dense(3, activation='softmax',  name='rate_out1')(dense)
model = tf.keras.models.Model(inputs=in_layers, outputs=[output1, output2])
model.summary()

# tf.keras.utils.plot_model(model, 'multi_input_and_output_model.png', show_shapes=True)

# compile the keras model
#%%
# convert integers to dummy variables (i.e. one hot encoded)
train_labels_one_hot1 = tf.keras.utils.to_categorical(train_labels1.values)[:, 1:]
val_labels_one_hot1 = tf.keras.utils.to_categorical(val_labels1.values)[:, 1:]
train_labels_one_hot2 = tf.keras.utils.to_categorical(train_labels2.values)
val_labels_one_hot2 = tf.keras.utils.to_categorical(val_labels2.values)
#%% callbacks
run_path = "model_weights/concat_two_out"
earlyStopping = EarlyStopping(monitor='val_accuracy', patience=10, verbose=0)
# mcp_save = ModelCheckpoint(os.path.join(run_path, 'model-{epoch:03d}-{accuracy:03.4f}-{val_accuracy:03.4f}.h5'),
mcp_save = ModelCheckpoint(os.path.join(run_path, 'best_model.h5'),
                           save_best_only=True, monitor='val_accuracy')
reduce_lr_loss = ReduceLROnPlateau(monitor='val_accuracy', factor=0.1, patience=7, verbose=1, min_delta=1e-4)

callbacks = [earlyStopping, mcp_save, reduce_lr_loss]
#%%
# model.load_weights("model_weights/concat/model-050-0.929326-0.604333.h5")

# model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.compile(loss={'rate_out': 'categorical_crossentropy', 'rate_out1': 'categorical_crossentropy'},
              optimizer='adam',
              # metrics={'rate_out': ['accuracy'], 'rate_out1': ['accuracy']})
              metrics=['accuracy'])

#%%
batch_size = 128
num_epochs = 50
history = model.fit([X_train_enc[0], X_train_enc[1], train_padded],
                    [train_labels_one_hot1, train_labels_one_hot2],
                    epochs=num_epochs,
                    validation_data=([X_val_enc[0], X_val_enc[1], validation_padded], [val_labels_one_hot1,
                                                                                       val_labels_one_hot2]),
                    validation_steps=len(X_val_enc[0])/batch_size,
                    verbose=1,
                    # callbacks=callbacks,
                    batch_size=batch_size,
                    workers=6)


#%%
predictions = model.predict([X_val_enc[0], X_val_enc[1], validation_padded])